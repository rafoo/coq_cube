Require Import Arith.
Require Import Nat.

Require Import group.

(* Z / nZ is a group *)

Section modular.

  Variable n : nat.
  Hypothesis Hn : n <> 0.

  Definition Istrue (b : bool) := if b then True else False.

  Definition modular := {k : nat | Istrue (ltb k n)}.

  Definition modular_as_nat (m : modular) :=
    let (n, _) := m in n.

  Lemma ltb_mod k : Istrue (ltb (k mod n) n).
  Proof.
    case_eq (ltb (k mod n) n); simpl.
    - auto.
    - destruct (Nat.ltb_lt (k mod n) n) as (_, H).
      rewrite H.
      + discriminate.
      + apply Nat.mod_upper_bound.
        assumption.
  Qed.

  Definition mk_modular (k : nat) : modular :=
    exist _ (k mod n) (ltb_mod k).

  Lemma Istrue_irrel (b : bool) : forall H1 H2 : Istrue b, H1 = H2.
  Proof.
    case b.
    - intros [] []. reflexivity.
    - intro H; destruct H.
  Qed.

  Lemma modular_eq (m1 m2 : modular) : modular_as_nat m1 = modular_as_nat m2 -> m1 = m2.
  Proof.
    destruct m1 as (a, Ha).
    destruct m2 as (b, Hb).
    unfold modular_as_nat. simpl.
    intro He.
    apply eq_exist_uncurried.
    exists He.
    apply Istrue_irrel.
  Qed.

  Lemma mk_modular_eq (k1 k2 : nat) (H : k1 mod n = k2 mod n) : mk_modular k1 = mk_modular k2.
  Proof.
    apply modular_eq.
    unfold mk_modular, modular_as_nat.
    assumption.
  Qed.

  Lemma mk_modular_as_nat (m : modular) : mk_modular (modular_as_nat m) = m.
  Proof.
    unfold mk_modular, modular_as_nat.
    destruct m as (k, Hk).
    apply eq_exist_uncurried.
    assert (k mod n = k).
    - apply Nat.mod_small.
      apply Nat.ltb_lt.
      generalize Hk.
      case (k <? n).
      + reflexivity.
      + intro H; destruct H.
    - exists H.
      apply Istrue_irrel.
  Qed.

  Lemma make_modular_small i (Hi : Istrue (i <? n)) :
    mk_modular i = exist _ i Hi.
  Proof.
    unfold mk_modular.
    apply eq_exist_uncurried.
    assert (i < n) as Hin.
    - apply Nat.ltb_lt.
      apply Bool.Is_true_eq_true.
      assumption.
    - specialize (Nat.mod_small i n Hin).
      intro Himod.
      exists Himod.
      apply Istrue_irrel.
  Qed.

  Definition mod_plus (a b : modular) :=
    let (a', _) := a in
    let (b', _) := b in
    mk_modular (a' + b').

  Notation "a ++ b" := (mod_plus a b).

  Lemma mod_plus_plus a b : (mk_modular a) ++ (mk_modular b) = mk_modular (a + b).
  Proof.
    unfold mod_plus.
    apply mk_modular_eq.
    symmetry.
    apply Nat.add_mod.
    assumption.
  Qed.

  Lemma mod_plus_assoc a b c : a ++ (b ++ c) = (a ++ b) ++ c.
  Proof.
    rewrite <- (mk_modular_as_nat a).
    rewrite <- (mk_modular_as_nat b).
    rewrite <- (mk_modular_as_nat c).
    rewrite mod_plus_plus.
    rewrite mod_plus_plus.
    rewrite mod_plus_plus.
    rewrite mod_plus_plus.
    f_equal.
    apply plus_assoc.
  Qed.

  Lemma mod_plus_commutes a b : a ++ b = b ++ a.
  Proof.
    rewrite <- (mk_modular_as_nat a).
    rewrite <- (mk_modular_as_nat b).
    rewrite mod_plus_plus.
    rewrite mod_plus_plus.
    f_equal.
    symmetry.
    apply Nat.add_comm.
  Qed.

  Lemma mod_plus_zero a : a ++ (mk_modular 0) = a.
  Proof.
    rewrite <- (mk_modular_as_nat a).
    rewrite mod_plus_plus.
    f_equal.
    apply Nat.add_0_r.
  Qed.

  Lemma mod_zero_plus a : (mk_modular 0) ++ a = a.
    rewrite <- (mk_modular_as_nat a).
    rewrite mod_plus_plus.
    f_equal.
  Qed.

  Definition mod_opp (a : modular) :=
    let (a', _) := a in mk_modular (n - a').

  Lemma mod_plus_opp (a : modular) : a ++ mod_opp a = mk_modular 0.
  Proof.
    destruct a as (a', Ha).
    unfold mod_opp, mod_plus.
    apply mk_modular_eq.
    rewrite Nat.mod_0_l; try assumption.
    rewrite Nat.add_mod_idemp_r; try assumption.
    rewrite <- le_plus_minus.
    - apply Nat.mod_same.
      assumption.
    - apply Nat.lt_le_incl.
      apply Nat.ltb_lt.
      generalize Ha; case (ltb a' n); simpl; auto.
  Qed.

  Lemma mod_opp_plus (a : modular) : mod_opp a ++ a = mk_modular 0.
  Proof.
    rewrite mod_plus_commutes.
    apply mod_plus_opp.
  Qed.

  Lemma mod_eq_decide (a b : modular) : Decidable.decidable (a = b).
  Proof.
    case (Nat.eq_decidable (modular_as_nat a) (modular_as_nat b)).
    - intro He.
      left.
      apply (f_equal mk_modular) in He.
      rewrite mk_modular_as_nat in He.
      rewrite mk_modular_as_nat in He.
      assumption.
    - intro Hne.
      right.
      intro He.
      congruence.
  Qed.

  Definition modular_group : group :=
    mk_group modular mod_plus mod_opp (mk_modular 0) mod_plus_assoc mod_zero_plus mod_plus_zero mod_opp_plus mod_plus_opp mod_eq_decide.

End modular.
