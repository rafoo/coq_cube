Require Import face edge corner.

Definition cuby := (oriented_edge * oriented_corner)%type.

Definition cuby_move f (cb : cuby) : cuby :=
  let (oe, oc) := cb in
  (oe_move f oe, oc_move f oc).
