

Inductive face := F | R | U | B | L | D.

Definition face_eq f1 f2 :=
  match f1, f2 with
    | F, F
    | R, R
    | U, U
    | B, B
    | L, L
    | D, D => true
    | _, _ => false
  end.

Lemma face_eq_reflect f1 f2: face_eq f1 f2 = true <-> f1 = f2.
Proof.
  case f1; case f2; simpl; split; try discriminate; try congruence.
Defined.

Definition opp f :=
  match f with
    | F => B
    | R => L
    | U => D
    | B => F
    | L => R
    | D => U
  end.

Lemma opp_opp f : opp (opp f) = f.
Proof.
  case f; reflexivity.
Defined.

Definition parallel f1 f2 := f1 = f2 \/ opp (f1) = f2.

Lemma parallel_refl f : parallel f f.
Proof.
  left.
  reflexivity.
Defined.

Lemma opp_parallel f : parallel (opp f) f.
Proof.
  right.
  apply opp_opp.
Defined.

Lemma parallel_opp f : parallel f (opp f).
Proof.
  right.
  reflexivity.
Defined.

Lemma parallel_sym f1 f2 : parallel f1 f2 -> parallel f2 f1.
Proof.
  intros [[] | []].
  - apply parallel_refl.
  - apply opp_parallel.
Defined.

Lemma parallel_trans f1 f2 f3 : parallel f1 f2 -> parallel f2 f3 -> parallel f1 f3.
Proof.
  intros [[] | []] [[] | []];
  try (rewrite opp_opp);
  try (apply parallel_refl);
  try (apply parallel_opp).
Defined.

