Require Import face cuby Coq.Vectors.Vector.

Definition move := (face * nat)%type.

(* sequence n is the type of sequences of at most n moves *)
Inductive sequence : nat -> Set :=
| nil n : sequence n
| cons n : move -> sequence n -> sequence (S n).

(* Applying a move *)
Fixpoint fn_apply (f : face) (n : nat) (c : cuby) {struct n} : cuby :=
  match n with
    | 0 => c
    | S n => cuby_move f (fn_apply f n c)
  end.

Definition cuby_move (m : move) (c : cuby) : cuby :=
  let (f, n) := m in fn_apply f n c.

(* Applying a sequence *)
Fixpoint seq_apply {n} (s : sequence n) (c : cuby) {struct s} : cuby :=
  match s with
    | nil _ => c
    | cons n f s => seq_apply s (cuby_move f c)
  end.

(* Extensional sequence equality *)
Definition same_seq n1 n2 (s1 : sequence n1) (s2 : sequence n2) :=
  forall c, seq_apply s1 c = seq_apply s2 c.

Lemma same_seq_refl {n} s : same_seq n n s s.
Proof.
  intro.
  reflexivity.
Defined.

Lemma same_seq_sym {n1 n2} s1 s2 : same_seq n1 n2 s1 s2 -> same_seq n2 n1 s2 s1.
Proof.
  intros H c; symmetry; apply H.
Defined.

Lemma same_seq_trans {n1 n2 n3} s1 s2 s3 : same_seq n1 n2 s1 s2 -> same_seq n2 n3 s2 s3 -> same_seq n1 n3 s1 s3.
Proof.
  intros H1 H2 c.
  transitivity (seq_apply s2 c).
  - apply H1.
  - apply H2.
Defined.

Lemma same_seq_tail {n1 n2} s1 s2 f : same_seq n1 n2 s1 s2 -> same_seq (S n1) (S n2) (cons _ f s1) (cons _ f s2).
Proof.
  intros H c.
  simpl.
  apply H.
Defined.

(* God number theorem can be stated as follows:
   forall s, exists s' : sequence 20, same_seq _ _ s s' *)

Fixpoint sequence_incr l (s : sequence l) : sequence (S l) :=
  match s with
    | nil _ => nil _
    | cons _ m s => cons _ m (sequence_incr _ s)
  end.

Fixpoint combine l (s : sequence l) {struct s} : sequence l :=
  match s with
    | nil _ => nil _
    | cons _ (f, n) s =>
      match s in sequence l return sequence (S l) with
        | nil _ => cons _ (f, n) (nil _)
        | cons _ (f', n') s =>
          if (face_eq f f') then cons _ (f, n + n') (sequence_incr _ (combine _ s))
          else cons _ (f, n) (cons _ (f', n') (combine _ s))
      end
  end.

Lemma fn_apply_plus (f : face) (n n' : nat) (c : cuby) :
  fn_apply f n' (fn_apply f n c) = fn_apply f (n + n') c.
Proof.
  destruct n; simpl.
  - reflexivity.
  - induction n'; simpl.
    + rewrite <- plus_n_O.
      reflexivity.
    + f_equal.
      rewrite Plus.plus_comm.
      simpl.
      rewrite Plus.plus_comm.
      assumption.
Defined.

Lemma combine_plus (f : face) {l} (n n' : nat) (s' : sequence l) :
  same_seq _ _ (cons _ (f, n) (cons _ (f, n') s')) (cons _ (f, n + n') s').
Proof.
  intro c; simpl.
  f_equal.
  apply fn_apply_plus.
Defined.

Lemma inversion_sequence_0 s : s = nil 0.
Proof.
  refine (match s with nil 0 => _ end).
  reflexivity.
Defined.

Lemma inversion_sequence_S {l} s : s = nil (S l) \/ (exists f n (s' : sequence l), s = cons l (f, n) s').
Proof.
  refine (match s with nil (S l') => _ | cons l' (f, n) s' => _ end).
  - left; reflexivity.
  - right; exists f; exists n; exists s'; reflexivity.
Defined.

Lemma same_seq_incr {l} s : same_seq l _ s (sequence_incr _ s).
Proof.
  induction s.
  - intro c.
    reflexivity.
  - simpl.
    apply same_seq_tail.
    apply IHs.
Defined.

Fixpoint combine_same {l} s {struct s} : same_seq l _ s (combine _ s).
Proof.
  destruct s as [|l1 (f1, n1) [|l2 (f2, n2) s]]; simpl.
  - apply same_seq_refl.
  - apply same_seq_refl.
  - case_eq (face_eq f1 f2); simpl.
    + intro Hf.
      apply face_eq_reflect in Hf.
      subst f2.
      intro c.
      simpl.
      rewrite fn_apply_plus.
      eapply eq_trans; [apply (combine_same _ s _)|].
      apply same_seq_incr.
    + intros _.
      apply same_seq_tail.
      apply same_seq_tail.
      apply combine_same.
Qed.
