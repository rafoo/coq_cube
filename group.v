Require Decidable.

Record group := mk_group {
  G : Set;
  prod : G -> G -> G;
  inv : G -> G;
  unit : G;
  assoc : forall a b c, prod a (prod b c) = prod (prod a b) c;
  unit_left : forall a, prod unit a = a;
  unit_right : forall a, prod a unit = a;
  inv_left : forall a, prod (inv a) a = unit;
  inv_right : forall a, prod a (inv a) = unit;
  eq_decide : forall a b : G, Decidable.decidable (a = b)
}.

Section group.
  Variable g : group.
  Notation "1" := (unit g).
  Notation "a * b" := (prod g a b).
  Notation "a ⁻¹" := (inv g a) (at level 30).

  Theorem unit_right_unique : forall a, (forall b, b * a = b) -> a = 1.
  Proof.
    intros a H.
    rewrite <- (H 1).
    rewrite unit_left.
    reflexivity.
  Qed.

  Theorem unit_left_unique : forall a, (forall b, a * b = b) -> a = 1.
  Proof.
    intros a H.
    rewrite <- (H 1).
    rewrite unit_right.
    reflexivity.
  Qed.

  Theorem inv_left_unique : forall a b, a * b = 1 -> b = a⁻¹.
  Proof.
    intros a b H.
    apply (f_equal (fun x => a⁻¹ * x)) in H.
    rewrite unit_right in H.
    rewrite assoc in H.
    rewrite inv_left in H.
    rewrite unit_left in H.
    assumption.
  Qed.

  Theorem inv_right_unique : forall a b, a * b = 1 -> a = b⁻¹.
  Proof.
    intros a b H.
    apply (f_equal (fun x => x * b⁻¹)) in H.
    rewrite unit_left in H.
    rewrite <- assoc in H.
    rewrite inv_right in H.
    rewrite unit_right in H.
    assumption.
  Qed.

  Theorem inv_prod : forall a b, (a * b)⁻¹ = b⁻¹ * a⁻¹.
  Proof.
    intros a b.
    symmetry.
    apply inv_left_unique.
    rewrite <- assoc.
    rewrite (assoc _ b (b⁻¹) (a⁻¹)).
    rewrite inv_right.
    rewrite unit_left.
    apply inv_right.
  Qed.

  Theorem inv_inv : forall a, (a⁻¹)⁻¹ = a.
  Proof.
    intro a.
    symmetry.
    apply inv_left_unique.
    apply inv_left.
  Qed.

  Theorem inv_unit : 1⁻¹ = 1.
  Proof.
    symmetry.
    apply inv_left_unique.
    apply unit_left.
  Qed.

End group.
