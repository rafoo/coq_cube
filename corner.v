Require Import face.

Inductive corner := FRU | FDR | FLD | FUL | BUR | BRD | BDL | BLU.

Definition corner_F c :=
  match c with
    | FRU => FDR
    | FDR => FLD
    | FLD => FUL
    | FUL => FRU
    | c => c
  end.

Lemma corner_F4 c : corner_F (corner_F (corner_F (corner_F c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_B c :=
  match c with
    | BUR => BLU
    | BLU => BDL
    | BDL => BRD
    | BRD => BUR
    | c => c
  end.

Lemma corner_B4 c : corner_B (corner_B (corner_B (corner_B c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_R c :=
  match c with
    | FRU => BUR
    | BUR => BRD
    | BRD => FDR
    | FDR => FRU
    | c => c
  end.

Lemma corner_R4 c : corner_R (corner_R (corner_R (corner_R c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_L c :=
  match c with
    | FUL => BLU
    | BLU => BDL
    | BDL => FLD
    | FLD => FUL
    | c => c
  end.

Lemma corner_L4 c : corner_L (corner_L (corner_L (corner_L c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_U c :=
  match c with
    | FUL => BLU
    | BLU => BUR
    | BUR => FRU
    | FRU => FUL
    | c => c
  end.

Lemma corner_U4 c : corner_U (corner_U (corner_U (corner_U c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_D c :=
  match c with
    | BDL => FLD
    | BRD => BDL
    | FDR => BRD
    | FLD => FDR
    | c => c
  end.

Lemma corner_D4 c : corner_D (corner_D (corner_D (corner_D c))) = c.
Proof.
  case c; reflexivity.
Defined.

Definition corner_move f :=
  match f with
    | F => corner_F
    | R => corner_R
    | U => corner_U
    | B => corner_B
    | L => corner_L
    | D => corner_D
  end.

Lemma corner_move_4 f e : corner_move f (corner_move f (corner_move f (corner_move f e))) = e.
Proof.
  case f.
  - apply corner_F4.
  - apply corner_R4.
  - apply corner_U4.
  - apply corner_B4.
  - apply corner_L4.
  - apply corner_D4.
Defined.

Lemma corner_move_parallel f1 f2 e : parallel f1 f2 -> corner_move f1 (corner_move f2 e) = corner_move f2 (corner_move f1 e).
Proof.
  intros [[] | []].
  - reflexivity.
  - destruct f1; destruct e; reflexivity.
Defined.

Inductive corner_orientation := CO0 | CO1 | CO2.
Definition next co :=
  match co with
    | CO0 => CO1
    | CO1 => CO2
    | CO2 => CO0
  end.
Definition pred co :=
  match co with
    | CO0 => CO2
    | CO1 => CO0
    | CO2 => CO1
  end.

Definition oriented_corner := (corner * corner_orientation)%type.
(* An oriented corner is defined by its position (of type corner)
   and its orientation (of type corner_orientation). *)

Definition co_F c :=
  match c with
    | FDR | FUL => CO1
    | FRU | FLD => CO2
    | _ => CO0
  end.

Definition co_B c :=
  match c with
    | BUR | BDL => CO1
    | BRD | BLU => CO2
    | _ => CO0
  end.

Definition co_R c :=
  match c with
    | FDR | BUR => CO1
    | BRD | FRU => CO2
    | _ => CO0
  end.

Definition co_L c :=
  match c with
    | BDL | FUL => CO1
    | FLD | BLU => CO2
    | _ => CO0
  end.

Definition co_move f :=
  match f with
    | F => co_F
    | B => co_B
    | R => co_R
    | L => co_L
    | _ => fun e => CO0
  end.

Definition co_plus co1 co2 :=
  match co1 with
    | CO0 => co2
    | CO1 => next co2
    | CO2 => pred co2
  end.

Definition oc_move f (oc : oriented_corner) : oriented_corner :=
  let (c, co) := oc in
  (corner_move f c, co_plus co (co_move f c)).
