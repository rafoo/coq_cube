Require Import face.

Inductive edge := FR | FU | FL | FD | BR | BU | BL | BD | RU | LU | RD | LD.

Definition edge_F e :=
  match e with
    | FR => FD
    | FD => FL
    | FL => FU
    | FU => FR
    | e => e
  end.

Lemma edge_F4 e : edge_F (edge_F (edge_F (edge_F e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_B e :=
  match e with
    | BR => BU
    | BU => BL
    | BL => BD
    | BD => BR
    | e => e
  end.

Lemma edge_B4 e : edge_B (edge_B (edge_B (edge_B e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_R e :=
  match e with
    | FR => RU
    | RU => BR
    | BR => RD
    | RD => FR
    | e => e
  end.

Lemma edge_R4 e : edge_R (edge_R (edge_R (edge_R e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_L e :=
  match e with
    | FL => LD
    | LD => BL
    | BL => LU
    | LU => FL
    | e => e
  end.

Lemma edge_L4 e : edge_L (edge_L (edge_L (edge_L e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_U e :=
  match e with
    | FU => LU
    | LU => BU
    | BU => RU
    | RU => FU
    | e => e
  end.

Lemma edge_U4 e : edge_U (edge_U (edge_U (edge_U e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_D e :=
  match e with
    | FD => RD
    | RD => BD
    | BD => LD
    | LD => FD
    | e => e
  end.

Lemma edge_D4 e : edge_D (edge_D (edge_D (edge_D e))) = e.
Proof.
  case e; reflexivity.
Defined.

Definition edge_move f :=
  match f with
    | F => edge_F
    | R => edge_R
    | U => edge_U
    | B => edge_B
    | L => edge_L
    | D => edge_D
  end.

Lemma edge_move_4 f e : edge_move f (edge_move f (edge_move f (edge_move f e))) = e.
Proof.
  case f.
  - apply edge_F4.
  - apply edge_R4.
  - apply edge_U4.
  - apply edge_B4.
  - apply edge_L4.
  - apply edge_D4.
Defined.

Lemma edge_move_parallel f1 f2 e : parallel f1 f2 -> edge_move f1 (edge_move f2 e) = edge_move f2 (edge_move f1 e).
Proof.
  intros [[] | []].
  - reflexivity.
  - destruct f1; destruct e; reflexivity.
Defined.


Inductive edge_orientation := EO0 | EO1.
Definition swap eo :=
  match eo with
    | EO0 => EO1
    | EO1 => EO0
  end.

Definition oriented_edge := (edge * edge_orientation)%type.
(* An oriented edge is defined by its position (of type edge)
   and its orientation (of type edge_orientation). *)

(* The F move disorients all edges on the F face and nothing else *)
Definition eo_F e :=
  match e with
    | FR | FU | FL | FD => EO1
    | _ => EO0
  end.

(* The B move disorients all edges on the B face and nothing else *)
Definition eo_B e :=
  match e with
    | BR | BU | BL | BD => EO1
    | _ => EO0
  end.

(* Moves on the 4 other faces don't change edge orientation *)

Definition eo_move f :=
  match f with
    | F => eo_F
    | B => eo_B
    | _ => fun e => EO0
  end.

Definition eo_plus eo1 eo2 :=
  match eo1 with
    | EO0 => eo2
    | EO1 => swap eo2
  end.

Definition oe_move f (oe : oriented_edge) : oriented_edge :=
  let (e, eo) := oe in
  (edge_move f e, eo_plus eo (eo_move f e)).
