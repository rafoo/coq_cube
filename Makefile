
all: face.vo edge.vo corner.vo cuby.vo sequence.vo group.vo modular_arith.vo permutation.vo

%.vo: %.v
	coqc $<

edge.vo: face.vo
corver.vo: face.vo
cuby.vo: face.vo edge.vo corner.vo
sequence.vo: face.vo edge.vo corner.vo cuby.vo
modular_arith.vo: group.vo
permutation.vo: group.vo modular_arith.vo

clean:
	rm -f *.vo .*.aux *.glob
