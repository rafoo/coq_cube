Require Import group modular_arith.
Require Import Arith.
Require Import List.
Require Import Bool.
Require Import Lia.
Require Decidable.
Require Vector.

Section permutation.

  Record permutation_group :=
    mk_permutation_group {
        G : Set;
        A : Set;
        id : G;
        comp : G -> G -> G;
        inv : G -> G;
        apply : G -> A -> A;
        Hid : forall x : A, apply id x = x;
        Hcomp : forall s1 s2 x,
            apply (comp s1 s2) x = apply s2 (apply s1 x);
        Hinv1 : forall s, comp (inv s) s = id;
        Hinv2 : forall s, comp s (inv s) = id;
        ext : forall s1 s2, (forall x, apply s1 x = apply s2 x) -> s1 = s2;
        eq_decide : forall s1 s2 : G, Decidable.decidable (s1 = s2)
      }.

  Lemma permutation_group_is_group (G : permutation_group) : group.
  Proof.
    destruct G as (G, A, id, comp, inv, apply, Hid, Hcomp, Hinv1, Hinv2, ext, eq_decide).
    apply (mk_group G comp inv id).
    - intros s1 s2 s3.
      apply ext.
      intro x.
      repeat rewrite Hcomp.
      reflexivity.
    - intro s.
      apply ext.
      intro x.
      rewrite Hcomp.
      rewrite Hid.
      reflexivity.
    - intro s.
      apply ext.
      intro x.
      rewrite Hcomp.
      rewrite Hid.
      reflexivity.
    - assumption.
    - assumption.
    - assumption.
  Defined.

End permutation.

Section finite.
  Variable n : nat.
  Hypothesis Hn : n <> 0.

  Fixpoint for_all k (v : Vector.t (modular n) k) (p : modular n -> bool) :=
    match v with
    | Vector.nil _ => true
    | Vector.cons _ a _ v => p a && for_all _ v p
    end.

  Fixpoint get k (v : Vector.t (modular n) k) : modular k -> modular n :=
    match v in Vector.t _ k return modular k -> modular n with
    | Vector.nil _ => fun x => False_rec _ (proj2_sig x)
    | Vector.cons _ a k v => fun x =>
      match x with
      | exist _ 0 _ => a
      | exist _ (S i) H => get k v (exist _ i H)
      end
    end.

  Lemma get_ext k (v1 v2 : Vector.t (modular n) k) :
    (forall x, get k v1 x = get k v2 x) -> v1 = v2.
  Proof.
    apply (Vector.rect2
             (fun k v1 v2 =>
                (forall x, get k v1 x = get k v2 x) ->
                v1 = v2)).
    - reflexivity.
    - clear k v1 v2.
      intros k v1 v2 IH a1 a2 H.
      assert (a1 = a2).
      + specialize (H (exist _ 0 I)).
        simpl in H.
        auto.
      + subst a2.
        f_equal.
        apply IH.
        intros (i, Hi).
        exact (H (exist _ (S i) Hi)).
  Qed.

  Definition modular_fun := Vector.t (modular n) n.

  Definition modular_apply (f : modular_fun) (x : modular n) : modular n :=
    get n f x.

  Lemma modular_fun_ext (f g : modular_fun) :
    (forall x, modular_apply f x = modular_apply g x) -> f = g.
  Proof.
    apply get_ext.
  Qed.

  Lemma ltb_ltb_succ_r i j : Is_true (i <? j) -> Is_true (i <? S j).
  Proof.
    intro H.
    apply Is_true_eq_left.
    apply Nat.ltb_lt.
    apply Nat.lt_lt_succ_r.
    apply Nat.ltb_lt.
    apply Is_true_eq_true.
    assumption.
  Qed.

  Lemma ltb_succ_diag_r i : Is_true (i <? S i).
  Proof.
    apply Is_true_eq_left.
    apply Nat.ltb_lt.
    apply Nat.lt_succ_diag_r.
  Qed.

  Lemma ltb_succ_destruct i j : Is_true (i <? S j) -> i = j \/ Is_true (i <? j).
  Proof.
    intro H.
    apply Is_true_eq_true in H.
    apply Nat.ltb_lt in H.
    apply lt_n_Sm_le in H.
    apply le_lt_or_eq in H.
    destruct H.
    - right.
      apply Is_true_eq_left.
      apply Nat.ltb_lt.
      assumption.
    - auto.
  Qed.

  Fixpoint forall_lt k : (forall i, Is_true (i <? k) -> bool) -> bool :=
    match k with
    | 0 => fun _ => true
    | S k => fun p =>
               forall_lt k (fun i Hi => p i (ltb_ltb_succ_r _ _ Hi)) &&
               (p k (ltb_succ_diag_r k))
    end.

  Lemma forall_lt_correct k p :
    forall_lt k p = true -> forall i Hi, p i Hi = true.
  Proof.
    generalize p; clear p.
    induction k; intro p; simpl.
    - intros _ k Hk.
      contradiction.
    - intros Hflt i Hi.
      apply andb_prop in Hflt.
      destruct Hflt as (Hflt, Hk).
      case (ltb_succ_destruct _ _ Hi).
      * intro; subst i.
        rewrite <- Hk.
        f_equal.
        apply Istrue_irrel.
      * intro Hi'.
        specialize (IHk _ Hflt i Hi').
        simpl in IHk.
        rewrite <- IHk.
        f_equal.
        apply Istrue_irrel.
  Qed.

  Lemma forall_lt_complete k p :
    (forall i Hi, p i Hi = true) -> forall_lt k p = true.
  Proof.
    generalize p; clear p.
    induction k; intro p; simpl.
    - reflexivity.
    - intro Hflt.
      rewrite (Hflt k _).
      rewrite IHk.
      + reflexivity.
      + intros; apply Hflt.
  Qed.

  Definition modular_eq (i j : modular n) : bool :=
    modular_as_nat n i =? modular_as_nat n j.

  Lemma modular_eq_correct i j : modular_eq i j = true -> i = j.
  Proof.
    intro H.
    apply modular_arith.modular_eq.
    apply beq_nat_true.
    exact H.
  Qed.

  Lemma modular_eq_complete i j : i = j -> modular_eq i j = true.
  Proof.
    intro; subst j.
    apply Nat.eqb_refl.
  Qed.

  Fixpoint vector_snoc {A : Type} {n} (v : Vector.t A n) (a : A) : Vector.t A (S n) :=
    match v with
    | Vector.nil _ => Vector.cons A a 0 (Vector.nil A)
    | Vector.cons _ b n v =>
      Vector.cons A b (S n) (vector_snoc v a)
    end.

  Lemma Is_true_leb_S_left a b : Is_true (S a <=? b) -> Is_true (a <=? b).
  Proof.
    intro H.
    apply Is_true_eq_left.
    apply Nat.leb_le.
    apply Is_true_eq_true in H.
    apply Nat.leb_le in H.
    lia.
  Qed.

  Fixpoint vect_of_fun k (f : modular n -> modular n) :
    Is_true (k <=? n) -> Vector.t (modular n) k :=
    match k with
    | 0 => fun _ => Vector.nil _
    | S k => fun Hk =>
      vector_snoc
        (vect_of_fun k f (Is_true_leb_S_left _ _ Hk))
        (f (exist _ k Hk))
    end.

  Lemma get_snoc_last k (v : Vector.t _ k) a Hk :
    get (S k) (vector_snoc v a) (exist _ k Hk) = a.
  Proof.
    induction v; simpl.
    - reflexivity.
    - apply IHv.
  Qed.

  Lemma get_snoc_not_last k (v : Vector.t _ k) a i Hi Hi' :
    get (S k) (vector_snoc v a) (exist _ i Hi) =
    get k v (exist _ i Hi').
  Proof.
    generalize i Hi Hi'; clear i Hi Hi'.
    induction v; simpl; intros i Hi Hi'.
    - contradiction.
    - destruct i.
      + reflexivity.
      + apply IHv.
  Qed.

  Lemma ltb_leb_trans {a b c} :
    Is_true (a <? b) -> Is_true (b <=? c) -> Is_true (a <? c).
  Proof.
    intros H1 H2.
    apply Is_true_eq_left.
    apply Nat.ltb_lt.
    apply Is_true_eq_true in H1.
    apply Nat.ltb_lt in H1.
    apply Is_true_eq_true in H2.
    apply Nat.leb_le in H2.
    lia.
  Qed.

  Lemma get_vect_of_fun k f i Hi Hk Hin :
    get k (vect_of_fun k f Hk) (exist _ i Hi) = f (exist _ i Hin).
  Proof.
    generalize i Hi Hin; clear i Hi Hin.
    induction k; simpl; intros i Hi Hin.
    - contradiction.
    - specialize (ltb_succ_destruct _ _ Hi); intros [Hi'|Hi'].
      + subst i.
        rewrite (get_snoc_last k _ _ Hi).
        f_equal.
        f_equal.
        apply Istrue_irrel.
      + rewrite (get_snoc_not_last k _ _ i Hi Hi').
        apply IHk.
  Qed.

  Definition modular_fun_from_fun (f : modular n -> modular n) : modular_fun :=
    vect_of_fun n f (Is_true_eq_left _ (Nat.leb_refl n)).

  Definition modular_fun_id := modular_fun_from_fun (fun i => i).

  Lemma modular_apply_from_fun f i :
    modular_apply (modular_fun_from_fun f) i = f i.
  Proof.
    unfold modular_fun_id, modular_apply, modular_fun_from_fun.
    destruct i as (i, Hi).
    rewrite (get_vect_of_fun n f i Hi _ Hi).
    reflexivity.
  Qed.

  Lemma modular_from_fun_apply f :
    modular_fun_from_fun (modular_apply f) = f.
  Proof.
    apply modular_fun_ext.
    intro x.
    apply modular_apply_from_fun.
  Qed.

  Lemma modular_fun_id_apply x : modular_apply modular_fun_id x = x.
  Proof.
    unfold modular_fun_id.
    rewrite modular_apply_from_fun.
    reflexivity.
  Qed.

  Definition modular_comp (f g : modular_fun) : modular_fun :=
    modular_fun_from_fun (fun x => modular_apply g (modular_apply f x)).

  Lemma modular_comp_apply f g x :
    modular_apply (modular_comp f g) x =
    modular_apply g (modular_apply f x).
  Proof.
    unfold modular_comp.
    rewrite modular_apply_from_fun.
    reflexivity.
  Qed.

  Definition modular_permutation :=
    { f : modular_fun & { g : modular_fun |
        Is_true
          (forall_lt n
                     (fun i Hi =>
                        let k : modular n := exist _ i Hi in
                        modular_eq (modular_apply f (modular_apply g k)) k &&
                        modular_eq (modular_apply g (modular_apply f k)) k)) }}.

  Definition mk_modular_permutation
             (f : modular n -> modular n)
             (g : modular n -> modular n)
             (Hfg : forall x, f (g x) = x)
             (Hgf : forall x, g (f x) = x) : modular_permutation.
  Proof.
    exists (modular_fun_from_fun f).
    exists (modular_fun_from_fun g).
    apply Is_true_eq_left.
    apply forall_lt_complete.
    intros i Hi.
    simpl.
    apply andb_true_intro.
    split; apply modular_eq_complete; do 2 rewrite modular_apply_from_fun; auto.
  Defined.

  Definition perm_apply (s : modular_permutation) (x : modular n) : modular n :=
    let (f, _) := s in modular_apply f x.

  Lemma perm_apply_mk f g Hfg Hgf x :
    perm_apply (mk_modular_permutation f g Hfg Hgf) x = f x.
  Proof.
    simpl.
    apply modular_apply_from_fun.
  Qed.

  Definition perm_inv (s : modular_permutation) : modular_permutation.
  Proof.
    destruct s as (f, (g, H)).
    exists g.
    exists f.
    apply Is_true_eq_left.
    apply Is_true_eq_true in H.
    apply forall_lt_complete.
    intros i Hi.
    rewrite andb_comm.
    exact (forall_lt_correct _ _ H i Hi).
  Defined.

  Definition perm_inv_apply (s : modular_permutation) x y :
    perm_apply (perm_inv s) y = x <->
    perm_apply s x = y.
  Proof.
    destruct s as (f, (g, H)).
    simpl.
    apply Is_true_eq_true in H.
    destruct x as (x, Hx).
    destruct y as (y, Hy).
    specialize (forall_lt_correct _ _ H x Hx); simpl.
    intro H2.
    specialize (forall_lt_correct _ _ H y Hy); simpl.
    intro H3.
    apply andb_prop in H2.
    apply andb_prop in H3.
    destruct H2 as (_, H2).
    apply modular_eq_correct in H2.
    destruct H3 as (H3, _).
    apply modular_eq_correct in H3.
    split; intro H4; rewrite <- H4; assumption.
  Qed.

  Definition perm_inv_comp_apply (s : modular_permutation) x :
    perm_apply (perm_inv s) (perm_apply s x) = x.
  Proof.
    rewrite perm_inv_apply.
    reflexivity.
  Qed.

  Definition perm_comp_inv_apply (s : modular_permutation) x :
    perm_apply s (perm_apply (perm_inv s) x) = x.
  Proof.
    rewrite <- perm_inv_apply.
    reflexivity.
  Qed.

  Lemma inv_mk f g Hfg Hgf :
    perm_inv (mk_modular_permutation f g Hfg Hgf) =
    mk_modular_permutation g f Hgf Hfg.
  Proof.
    unfold mk_modular_permutation, perm_inv.
    do 2 f_equal.
    apply Istrue_irrel.
  Qed.

  Definition perm_id : modular_permutation :=
    mk_modular_permutation
      (fun x => x)
      (fun x => x)
      (fun x => eq_refl)
      (fun x => eq_refl).

  Definition perm_comp (s1 s2 : modular_permutation) : modular_permutation.
  Proof.
    apply (mk_modular_permutation
             (fun x => perm_apply s2 (perm_apply s1 x))
             (fun x => perm_apply (perm_inv s1) (perm_apply (perm_inv s2) x))).
    - intro x.
      rewrite perm_comp_inv_apply.
      rewrite perm_comp_inv_apply.
      reflexivity.
    - intro x.
      rewrite perm_inv_comp_apply.
      rewrite perm_inv_comp_apply.
      reflexivity.
  Defined.

  Lemma perm_ext (s1 s2 : modular_permutation) :
    (forall x, perm_apply s1 x = perm_apply s2 x) -> s1 = s2.
  Proof.
    intro H.
    destruct s1 as (f1, (g1, H1)).
    destruct s2 as (f2, (g2, H2)).
    assert (f1 = f2) as Hf.
    - apply modular_fun_ext.
      apply H.
    - apply eq_existT_uncurried.
      exists Hf.
      subst f2.
      simpl.
      assert (g1 = g2) as Hg.
      + apply modular_fun_ext.
        intro x.
        transitivity (modular_apply g1 (modular_apply f1 (modular_apply g2 x))).
        * f_equal.
          symmetry.
          clear H H1.
          apply Is_true_eq_true in H2.
          destruct x as (x, Hx).
          specialize (forall_lt_correct _ _ H2 x Hx); simpl.
          intro H.
          apply andb_prop in H.
          destruct H as (H, _).
          apply modular_eq_correct in H.
          exact H.
        * clear H H2.
          apply Is_true_eq_true in H1.
          generalize (modular_apply g2 x).
          intros (y, Hy).
          specialize (forall_lt_correct _ _ H1 y Hy); simpl.
          intro H.
          apply andb_prop in H.
          destruct H as (_, H).
          apply modular_eq_correct in H.
          exact H.
      + apply eq_exist_uncurried.
        exists Hg.
        apply Istrue_irrel.
  Qed.

  Lemma perm_comp_apply s1 s2 x :
    perm_apply (perm_comp s1 s2) x =
    perm_apply s2 (perm_apply s1 x).
  Proof.
    unfold perm_comp.
    rewrite perm_apply_mk.
    reflexivity.
  Qed.

  Lemma perm_apply_id x : perm_apply perm_id x = x.
  Proof.
    unfold perm_id.
    apply perm_apply_mk.
  Qed.

  Lemma perm_inv_comp (s : modular_permutation) :
    perm_comp s (perm_inv s) = perm_id.
  Proof.
    apply perm_ext.
    intro x.
    rewrite perm_comp_apply.
    rewrite perm_inv_comp_apply.
    rewrite perm_apply_id.
    reflexivity.
  Qed.

  Lemma perm_comp_inv (s : modular_permutation) :
    perm_comp (perm_inv s) s = perm_id.
  Proof.
    apply perm_ext.
    intro x.
    rewrite perm_comp_apply.
    rewrite perm_comp_inv_apply.
    rewrite perm_apply_id.
    reflexivity.
  Qed.

  Lemma eq_false_imp_not b : b = false -> b <> true.
  Proof.
    intro; subst; discriminate.
  Qed.

  Lemma perm_decidable (s1 s2 : modular_permutation) : Decidable.decidable (s1 = s2).
  Proof.
    case_eq (forall_lt n
                       (fun i Hi =>
                          let x : modular n := exist _ i Hi in
                          modular_as_nat n (perm_apply s1 x) =?
                          modular_as_nat n (perm_apply s2 x))); intro H.
    - left.
      apply perm_ext.
      intros (x, Hx).
      specialize (forall_lt_correct _ _ H x Hx); simpl.
      intro H1.
      apply beq_nat_true in H1.
      apply modular_arith.modular_eq in H1.
      exact H1.
    - right; intro; subst s2.
      apply eq_false_imp_not in H.
      apply H; clear H.
      apply forall_lt_complete.
      intros i Hi.
      simpl.
      apply Nat.eqb_refl.
  Defined.

  Definition modular_permutation_group : permutation_group :=
    mk_permutation_group
      modular_permutation
      (modular n)
      perm_id
      perm_comp
      perm_inv
      perm_apply
      perm_apply_id
      perm_comp_apply
      perm_comp_inv
      perm_inv_comp
      perm_ext
      perm_decidable.

End finite.

